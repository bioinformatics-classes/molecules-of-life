## classes[1] = "The Molecules of Life"

#### Prespetivas em Bioinformática 2024-2025

![Logo EST](assets/logo-ESTB.png)


© Francisco Pina Martins 2018-2024

---

## What is life?

* &shy;<!-- .element: class="fragment" -->Controversial definitions  
  * &shy;<!-- .element: class="fragment" -->Physical entities
  * &shy;<!-- .element: class="fragment" -->Signaling & self-sustaining processes
  * &shy;<!-- .element: class="fragment" -->Associated with a specific environment
* &shy;<!-- .element: class="fragment" -->Viruses?
* &shy;<!-- .element: class="fragment" -->Synthetic life?

---

## DNA

![DNA](assets/DNA.jpg)

&shy;<!-- .element: class="fragment" -->**D**eoxyribo**n**ucleic **a**cid

---

## Where can we find DNA?

![Animal cell schematic](assets/animal-cell.jpg)

---

## Where can we find DNA?

![Plant cell schematic](assets/plant-cell.jpg)

---

## Where can we find DNA?

![Bacteria cell schematic](assets/bacteria-cell.jpg)

---

## Where can we find DNA?

![Virus representation](assets/virus.png)

---

<section data-background="assets/Cell01.png" data-background-size="1024px">

---

<section data-background="assets/Cell02.png" data-background-size="1024px">

---

## Nucleus

![Cell nucleus representation](assets/nucleus.png)

---

<section data-background="assets/Cell03.png" data-background-size="1024px">

---

## Mitochondria

![Mitochondria](assets/mitochondria.png)

---

<section data-background="assets/Cell04.png" data-background-size="1024px">

---

## Ribosome

![Ribossome](assets/ribosome_01.jpg)

---

<section data-background="assets/Cell05.png" data-background-size="1024px">

---

## DNA structure

---

<section data-background="assets/Chemical-structure-of-DNA.png" data-background-size="1024px">

---

<section data-background="assets/4-nucleotides.png" data-background-size="1024px">

---

<section data-background="assets/5-nucleotides.png" data-background-size="1024px">

---

## Nucleotides

![IUPAC](assets/IUPAC.png)

IUPAC codes

|||

## Nuclotides

<table BORDER CELLSPACING=0 CELLPADDING=2 COLS=2 WIDTH="600" style="font-size:50%">
<tr>
<td BGCOLOR="#B0C4DE"><font color="#000000">IUPAC nucleotide code</font></td>
<td BGCOLOR="#B0C4DE"><font color="#000000">Base</font></td>
</tr>
<tr>
<td>A</td>
<td>Adenine</td>
</tr>
<tr>
<td>C</td>
<td>Cytosine</td>
</tr>
<tr>
<td>G</td>
<td>Guanine</td>
</tr>
<tr>
<td>T (or U)</td>
<td>Thymine (or Uracil)</td>
</tr>
<tr>
<td>R</td>
<td>A or G</td>
</tr>
<tr>
<td>Y</td>
<td>C or T</td>
</tr>
<tr>
<td>S</td>
<td>G or C</td>
</tr>
<tr>
<td>W</td>
<td>A or T</td>
</tr>
<tr>
<td>K</td>
<td>G or T</td>
</tr>
<tr>
<td>M</td>
<td>A or C</td>
</tr>
<tr>
<td>B</td>
<td>C or G or T</td>
</tr>
<tr>
<td>D</td>
<td>A or G or T</td>
</tr>
<tr>
<td>H</td>
<td>A or C or T</td>
</tr>
<tr>
<td>V</td>
<td>A or C or G</td>
</tr>
<tr>
<td>N</td>
<td>any base</td>
</tr>
<tr>
<td>-</td>
<td>gap</td>
</tr>
</table>

---

<section data-background="assets/Phospahte-backbone.png" data-background-size="1024px">

---

<section data-background="assets/Watson-Crick-pair-rule.png" data-background-size="1024px">

---

<section data-background="assets/Sense-DNA.png" data-background-size="1024px">

---

<section data-background="assets/Sense-template.png" data-background-size="1024px">

---

## "Reading" DNA

&shy;<!-- .element: class="fragment" -->AKA "Transcription": DNA - RNA

&shy;<!-- .element: class="fragment" -->![Transcription](assets/transcription.png)

&shy;<!-- .element: class="fragment" -->This RNA is then moved towards a ribosome

---

## "Writing" DNA

&shy;<!-- .element: class="fragment" -->AKA "Translation": RNA - Protein

&shy;<!-- .element: class="fragment" -->![Translation](assets/ribosome_02.jpg)

&shy;<!-- .element: class="fragment" -->The RNA is translated into a protein

---

## STOP!

![STOP sign](assets/stop-sign.png)

---

## Central dogma of molecular biology

![Central dogma of molecular biology](assets/Central_dogma.png)

|||

## Central dogma

*This states that once 'information' has passed into protein it cannot get out again. In more detail, the transfer of information from nucleic acid to nucleic acid, or from nucleic acid to protein may be possible, but transfer from protein to protein, or from protein to nucleic acid is impossible. Information means here the precise determination of sequence, either of bases in the nucleic acid or of amino acid residues in the protein.*

<p align="right">- Francis Crick, 1958</p>

---

### Translation Genetic code

![Codon table](assets/codon_table.jpg)

|||

### Standard genetic code

</br>

[Codon table](https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi#SG1)

</br>

[Alternative translation table](https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi)

|||

### AA IUPAC codes

<table BORDER CELLSPACING=0 CELLPADDING=2 COLS=3 WIDTH="700" style="font-size:45%">
<tr>
<td BGCOLOR="#B0C4DE"><font color="#000000">IUPAC amino acid code</font></td>
<td BGCOLOR="#B0C4DE"><font color="#000000">Three letter code</font></td>
<td BGCOLOR="#B0C4DE"><font color="#000000">Amino acid</font></td>
</tr>
<tr>
<td>A</td>
<td>Ala</td>
<td>Alanine</td>
</tr>
<tr>
<td>C</td>
<td>Cys</td>
<td>Cysteine</td>
</tr>
<tr>
<td>D</td>
<td>Asp</td>
<td>Aspartic Acid</td>
</tr>
<tr>
<td>E</td>
<td>Glu</td>
<td>Glutamic Acid</td>
</tr>
<tr>
<td>F</td>
<td>Phe</td>
<td>Phenylalanine</td>
</tr>
<tr>
<td>G</td>
<td>Gly</td>
<td>Glycine</td>
</tr>
<tr>
<td>H</td>
<td>His</td>
<td>Histidine</td>
</tr>
<tr>
<td>I</td>
<td>Ile</td>
<td>Isoleucine</td>
</tr>
<tr>
<td>K</td>
<td>Lys</td>
<td>Lysine</td>
</tr>
<tr>
<td>L</td>
<td>Leu</td>
<td>Leucine</td>
</tr>
<tr>
<td>M</td>
<td>Met</td>
<td>Methionine</td>
</tr>
<tr>
<td>N</td>
<td>Asn</td>
<td>Asparagine</td>
</tr>
<tr>
<td>P</td>
<td>Pro</td>
<td>Proline</td>
</tr>
<tr>
<td>Q</td>
<td>Gln</td>
<td>Glutamine</td>
</tr>
<tr>
<td>R</td>
<td>Arg</td>
<td>Arginine</td>
</tr>
<tr>
<td>S</td>
<td>Ser</td>
<td>Serine</td>
</tr>
<tr>
<td>T</td>
<td>Thr</td>
<td>Threonine</td>
</tr>
<tr>
<td>V</td>
<td>Val</td>
<td>Valine</td>
</tr>
<tr>
<td>W</td>
<td>Trp</td>
<td>Tryptophan</td>
</tr>
<tr>
<td>Y</td>
<td>Tyr</td>
<td>Tyrosine</td>
</tr>
</table>

|||

## Start & Stop

* &shy;<!-- .element: class="fragment" -->The 1<sup>st</sup> amino acid of any protein is always `Met`
  * &shy;<!-- .element: class="fragment" -->`Met` is also called a "start codon"
  * &shy;<!-- .element: class="fragment" -->`Met` is encoded by the nucleotide sequence `ATG`
* &shy;<!-- .element: class="fragment" -->In order to finish a protein, a `STOP` codon is used
  * &shy;<!-- .element: class="fragment" -->`TAA`
  * &shy;<!-- .element: class="fragment" -->`TAG`
  * &shy;<!-- .element: class="fragment" -->`TGA`

|||

## Frames

There are 6 possible reading frames:

```
Frm|ATGATCTGCTAA|
-----------------
 1 |MetIleSerSTP|
 2 | STPSerAlaXX|
 3 |  AspLeuLeuX|
----------------- 
-1 |AspLeuArgAsn|
-2 |XXSTPValIle |
-3 |XSerSerSer  |
```

* &shy;<!-- .element: class="fragment" -->[More details here](http://bioweb.uwlax.edu/GenWeb/Molecular/Seq_Anal/Translation/translation.html)

---

## DNA Electrophoresis


* &shy;<!-- .element: class="fragment" -->DNA is negatively charged
  * &shy;<!-- .element: class="fragment" -->Placing it in an electric field will make it migrate towards the (+) pole
  * &shy;<!-- .element: class="fragment" -->Inside a viscous medium, different sized molecules migrate at different speed

&shy;<!-- .element: class="fragment" -->![Electrophoresis gel](assets/gel.png)

---

## In Vitro DNA replication

* &shy;<!-- .element: class="fragment" -->PCR
  * &shy;<!-- .element: class="fragment" -->Polymerase Chain Reaction
  * &shy;<!-- .element: class="fragment" -->**Amplifies** a specific DNA region

</br>

<iframe class="fragment" width="560" height="315" src="https://www.youtube-nocookie.com/embed/JmveVAYKylk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|||

## What can it be used for?

* &shy;<!-- .element: class="fragment" -->Sequencing
* &shy;<!-- .element: class="fragment" -->Genotyping
* &shy;<!-- .element: class="fragment" -->Hybridization (Southern/Northern blot)
* &shy;<!-- .element: class="fragment" -->Measuring gene expression (Quantitative PCR)
* &shy;<!-- .element: class="fragment" -->Pathogen detection (Quantitative PCR)

---

## Sequencing DNA

Sanger Method

![Sanger sequencing](assets/sanger.jpg)

&shy;<!-- .element: class="fragment" -->[More information](https://doi.org/10.1111/febs.14319)

|||

## Sequencing DNA

Illumina method

![Illumina sequencing](assets/Illumina.png)

&shy;<!-- .element: class="fragment" -->[More information](https://bioinformaticamente.com/2020/11/13/illumina-sequencing/)

|||

## Sequencing DNA

Oxford Nanopore

![Oxford nanopore sequencing](assets/nanopore.jpg)

&shy;<!-- .element: class="fragment" -->[More information](https://nanoporetech.com/platform/technology)

---

## Summary

* &shy;<!-- .element: class="fragment" -->Where is DNA?
* &shy;<!-- .element: class="fragment" -->DNA structure
* &shy;<!-- .element: class="fragment" -->DNA representation
* &shy;<!-- .element: class="fragment" -->Transcription & translation
* &shy;<!-- .element: class="fragment" -->Central dogma of molecular biology
* &shy;<!-- .element: class="fragment" -->Standard genetic code
* &shy;<!-- .element: class="fragment" -->DNA Sequencing

---

## Homework

* Write a 21 nucleotide DNA sequence, where 4 of the bases represent ambiguities
* Re-write 3 possible unambiguous sequences based on the previous one
* Pick one of these sequences and write:
  * Its *reverse*
  * Its *complement*
  * Its *reverse-complement*
  * The amino-acid chain it produces 
    * **Use the 1 letter IUPAC encoding**
    * **Use the 'standard' genetic translation**

|||

## Homework

* Due date: Next class
* Delivery: email
* Format:
  * `.txt` file
  * `Your_name_Student_Number.txt`
    * Eg: `Francisco_Pina_Martins_29609.txt`

```
>Sequence identifier
ATGCTCGATCG...
>Another sequence identifier
GCTATCTGACT...
```

---

## References

* [NCBI DNA codon table](http://bioweb.uwlax.edu/GenWeb/Molecular/Seq_Anal/Translation/translation.html)
* [DNA Structure](https://doi.org/10.1111/febs.13307)
* [Gel electrophoresis](https://dx.doi.org/10.3791%2F3923)
* [Sanger sequencing](https://doi.org/10.1111/febs.14319)
* [Illumina sequencing](https://bioinformaticamente.com/2020/11/13/illumina-sequencing/)
* [Oxford Nanopore sequencing](https://nanoporetech.com/platform/technology)
